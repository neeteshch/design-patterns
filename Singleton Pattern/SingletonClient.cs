﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Singleton_Pattern
{
    class SingletonClient
    {
        public void CreateSingletonClient()
        {
            Parallel.Invoke(
                () => PrintTeacherDetails(),
                () => PrintStudentdetails()
                );
            Console.ReadLine();
            
        }

        private static void PrintTeacherDetails()
        {
            SingletonClass fromTeacher = SingletonClass.GetInstance;
            fromTeacher.PrintDetails("From Teacher");
        }
        private static void PrintStudentdetails()
        {
            SingletonClass fromStudent = SingletonClass.GetInstance;
            fromStudent.PrintDetails("From Student");
        }
    }
}
