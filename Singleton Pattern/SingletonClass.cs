﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Singleton_Pattern
{
    public sealed class SingletonClass
    {
        private static int counter = 0;
        private static SingletonClass instance = null;
        private static readonly object Instancelock = new object();

        public static SingletonClass GetInstance
        {
            /**/
            /*get
            {
                lock (Instancelock)
                {
                    if (instance == null)
                    {
                        instance = new SingletonClass();
                    }
                    return instance;
                }
            }*/

            /*Double-checked locking mechanism*/
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new SingletonClass();
                        }
                    }
                }
                return instance;
            }
        }
        private SingletonClass()
        {
            counter++;
            Console.WriteLine("Counter Value " + counter.ToString());
        }
        public void PrintDetails(string message)
        {
            Console.WriteLine(message);
        }
    }
}
