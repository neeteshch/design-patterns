﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.InheritanceObjectExample
{
    class InheritanceObjectExampleClass
    {
        public void CreateInheritanceObjectClient()
        {
            BaseA bas = new BaseA();
            bas.Base();

            BaseA der = new DerivedB();
            der.Base();

            BaseA basC = new DerivedC();
            basC.Base();

            Console.Read();
        }
    }

    class BaseA
    {
        public virtual void Base()
        {
            Console.WriteLine("BaseA method called");
        }
    }

    class DerivedB : BaseA
    {
        public override void Base()
        {
            Console.WriteLine("DerivedB method called");
        }
    }

    class DerivedC : BaseA
    {
        public new void Base()
        {
            Console.WriteLine("DerivedC method called");
        }
    }
}
