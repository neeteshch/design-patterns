﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern
{
    class LionConcreteProduct : IAnimal
    {
        public string Speak()
        {
            return "Roar";
        }
    }
}
