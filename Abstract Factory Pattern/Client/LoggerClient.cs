﻿using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Factory;
using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern.Client
{
    class LoggerClient
    {
        public void CreateClient()
        {
            ILog log = null;
            LogFactory logFactory = null;

            logFactory = LogFactory.CreateLogFactory("DB");
            log = logFactory.GetLog("SQL");
            log.WriteLogs();

            log = logFactory.GetLog("ORACLE");
            log.WriteLogs();

            logFactory = LogFactory.CreateLogFactory("File");

            log = logFactory.GetLog("Text");
            log.WriteLogs();

            log = logFactory.GetLog("CSV");
            log.WriteLogs();
            Console.Read();
        }
    }
}
