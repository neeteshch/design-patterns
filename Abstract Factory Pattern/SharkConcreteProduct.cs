﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern
{
    class SharkConcreteProduct: IAnimal
    {
        public string Speak()
        {
            return "Cannot Speak";
        }
    }
}
