﻿using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern.Concrete_Products
{
    class CSVFileLogCreation : ILog
    {
        public void WriteLogs()
        {
            Console.WriteLine("Logs will be written to CSV Files...");
        }
    }
}
