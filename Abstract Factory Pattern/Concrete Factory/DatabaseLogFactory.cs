﻿using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Factory;
using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Product;
using ConsoleApp1.Abstract_Factory_Pattern.Concrete_Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern.Concrete_Factory
{
    class DatabaseLogFactory : LogFactory
    {
        private ILog iLog;
        public override ILog GetLog(string LogType)
        {
            if (LogType == "SQL")
            {
                return new SQLDBLogCreation(); 
            }
            else if (LogType == "ORACLE")
            {
                return new OracleDBLogCreation();
            }
            else return null;
        }
    }
}
