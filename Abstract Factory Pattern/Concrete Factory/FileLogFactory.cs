﻿using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Factory;
using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Product;
using ConsoleApp1.Abstract_Factory_Pattern.Concrete_Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern.Concrete_Factory
{
    class FileLogFactory : LogFactory
    {
        public override ILog GetLog(string LogType)
        {
            if (LogType == "Text")
            {
                return new TextFileLogCreation();
            }
            else if (LogType == "CSV")
            {
                return new CSVFileLogCreation();
            }
            else return null;
        }
    }
}
