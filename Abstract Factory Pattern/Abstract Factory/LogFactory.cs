﻿using ConsoleApp1.Abstract_Factory_Pattern.Abstract_Product;
using ConsoleApp1.Abstract_Factory_Pattern.Concrete_Factory;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern.Abstract_Factory
{
    public abstract class LogFactory
    {
        private ILog iLog;
        public abstract ILog GetLog(string LogType);
        public static LogFactory CreateLogFactory(string FactoryType)
        {
            if (FactoryType.Equals("DB"))
                return new DatabaseLogFactory();
            else
                return new FileLogFactory();
        }
    }
}
