﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern
{
    class CatConcreteProduct: IAnimal
    {
        public string Speak()
        {
            return "Meow Meow Meow";
        }
    }
}
