﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern
{
    // creating an abstract product..
    public interface IAnimal
    {
        string Speak();
    }
}
