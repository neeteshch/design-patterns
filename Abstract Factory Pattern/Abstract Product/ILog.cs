﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern.Abstract_Product
{
    public interface ILog
    {
        void WriteLogs();
    }
}
