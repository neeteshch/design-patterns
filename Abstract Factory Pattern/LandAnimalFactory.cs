﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern
{
    public class LandAnimalFactory : AnimalFactory
    {
        public override IAnimal GetAnimal(string AnimalType)
        {
            if (AnimalType.Equals("Dog"))
            {
                return new DogConcreteProduct();
            }
            else if (AnimalType.Equals("Cat"))
            {
                return new CatConcreteProduct();
            }
            else if (AnimalType.Equals("Lion"))
            {
                return new LionConcreteProduct();
            }
            else
                return null;
        }
    }
}
