﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Abstract_Factory_Pattern
{
    public class SeaAnimalFactory : AnimalFactory
    {
        public override IAnimal GetAnimal(string AnimalType)
        {
            if (AnimalType.Equals("Shark"))
            {
                return new SharkConcreteProduct();
            }            
            else
                return null;
        }
    }
}
