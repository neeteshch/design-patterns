﻿using ConsoleApp1.Abstract_Factory_Pattern;
using ConsoleApp1.Abstract_Factory_Pattern.Client;
using ConsoleApp1.Factory_Pattern;
using ConsoleApp1.InheritanceObjectExample;
using ConsoleApp1.Singleton_Pattern;
using ConsoleApp1.Template_Method_Pattern;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //FactoryPatternClient factoryPatternClient = new FactoryPatternClient();
            //factoryPatternClient.CreateClient();

            //AbstractFactoryClient abstractFactoryClient = new AbstractFactoryClient();
            //abstractFactoryClient.CreateAbstractFactoryClient();

            //LoggerClient loggerClient = new LoggerClient();
            //loggerClient.CreateClient();

            //InheritanceObjectExampleClass objectExample = new InheritanceObjectExampleClass();
            //objectExample.CreateInheritanceObjectClient();

            //SingletonClient singletonClient = new SingletonClient();
            //singletonClient.CreateSingletonClient();

            //using (TemplateMethodClient templateMethodClient = new TemplateMethodClient() )
            //{
            //    templateMethodClient.CreateTemplateMethodClient();
            //}
            TemplateMethodClient templateMethodClient = new TemplateMethodClient();
            templateMethodClient.CreateTemplateMethodClient();
        }        
    }

    public abstract class XX

    {

        //public abstract int sum(int a, int b);
        //public abstract int subt(int a, int b);
        public void Display()
        {
            Console.WriteLine("Abstract class");
        }
    }

    public class AA
    {
        public virtual void BaseMethod()
        {
            Console.WriteLine("In Base Class AA");        
        }
    }

    public class BB:AA
    {
        public new void BaseMethod()
        {
            Console.WriteLine("In DerivedMethod BB");
        }
    }

    public class YY : XX
    {
        //public override int subt(int a, int b)
        //{
        //    return (a - b);
        //}

        //public override int sum(int a, int b)
        //{
        //    return (a + b);
        //}

        public void Show()
        {
            Console.WriteLine("YY class method");
        }
    }   
}


