﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Factory_Pattern
{
    // The Abstract Creator declares the factory method, which returns an object of type Product. 
    // this method is nothing but the factory method which will return the instance of the product.
    public abstract class CreditCardFactory
    {
        public abstract ICreditCard MakeProduct();

        public ICreditCard CreateProduct()
        {
            return this.MakeProduct();
        }
    }
}
