﻿using System;
using System.Collections.Generic;
using System.Text;
using static ConsoleApp1.Factory_Pattern.ConcreteCreator;
using static ConsoleApp1.Factory_Pattern.ConcreteProducts;

namespace ConsoleApp1.Factory_Pattern
{
    class FactoryPatternClient
    {
        // If you want to create an instance of the Platinum CreditCard then call the CreateProduct method of the PlatinumFactory instance, 
        // similarly, if you want the instance of Titanium CreditCard, then call the CreateProduct method of the TitaniumFactory instance.
        public void CreateClient()
        {
            ICreditCard creditCard = new PlatinumFactory().CreateProduct();
            //ICreditCard creditCard = new Platinum();
            if (creditCard != null)
            {
                Console.WriteLine("Card Type : " + creditCard.GetCardType());
                Console.WriteLine("Credit Limit : " + creditCard.GetCreditLimit());
                Console.WriteLine("Annual Charge :" + creditCard.GetAnnualCharge());
            }
            else
            {
                Console.Write("Invalid Card Type");
            }
            Console.WriteLine("--------------");

            creditCard = new MoneyBackFactory().CreateProduct();
            if (creditCard != null)
            {
                Console.WriteLine("Card Type : " + creditCard.GetCardType());
                Console.WriteLine("Credit Limit : " + creditCard.GetCreditLimit());
                Console.WriteLine("Annual Charge :" + creditCard.GetAnnualCharge());
            }
            else
            {
                Console.Write("Invalid Card Type");
            }
            Console.ReadLine();
        }
    }
}
