﻿using System;
using System.Collections.Generic;
using System.Text;
using static ConsoleApp1.Factory_Pattern.ConcreteProducts;

namespace ConsoleApp1.Factory_Pattern
{
    // The Concrete Creator object overrides the factory method to return an instance of a Concrete Product.
    class ConcreteCreator
    {
        public class MoneyBackFactory : CreditCardFactory
        {
            public override ICreditCard MakeProduct()
            {
                ICreditCard product = new MoneyBack();
                return product;
            }
        }
        public class PlatinumFactory : CreditCardFactory
        {
            public override ICreditCard MakeProduct()
            {
                ICreditCard product = new Platinum();
                return product;
            }
        }
        public class TitaniumFactory : CreditCardFactory
        {
            public override ICreditCard MakeProduct()
            {
                ICreditCard product = new Titanium();
                return product;
            }
        }
    }
}
