﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Factory_Pattern
{
    // This is going to be our Product interface which will provide the signature of the common functionalities
    // which should be implemented by the concrete product classes.
    public interface ICreditCard
    {
        string GetCardType();
        int GetCreditLimit();
        int GetAnnualCharge();
    }
}
