﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Factory_Pattern
{
    // This is a class implementing the Product interface.
    // this class will be responsible for CREATING CONCRETE PRODUCTS.
    class ConcreteProducts
    {
        public class Platinum : ICreditCard
        {
            public string GetCardType()
            {
                return "Platinum Plus";
            }
            public int GetCreditLimit()
            {
                return 35000;
            }
            public int GetAnnualCharge()
            {
                return 2000;
            }
        }

        public class Titanium : ICreditCard
        {
            public string GetCardType()
            {
                return "Titanium Edge";
            }
            public int GetCreditLimit()
            {
                return 25000;
            }
            public int GetAnnualCharge()
            {
                return 1500;
            }
        }
        public class MoneyBack : ICreditCard
        {
            public string GetCardType()
            {
                return "MoneyBack";
            }
            public int GetCreditLimit()
            {
                return 15000;
            }
            public int GetAnnualCharge()
            {
                return 500;
            }
        }
    }
}
