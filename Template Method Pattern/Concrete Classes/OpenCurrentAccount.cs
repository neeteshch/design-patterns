﻿using ConsoleApp1.Template_Method_Pattern.Template_Method;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Template_Method_Pattern.Concrete_Classes
{
    class OpenCurrentAccount : CreateAccountTemplate
    {
        protected override void FillForm()
        {
            Console.WriteLine("Current Account: Fill Form");
        }

        protected override void GetApproval()
        {
            Console.WriteLine("Current Account: Get Approval");
        }

        protected override void OpenAccount()
        {
            Console.WriteLine("Current Account: Open Account");
        }

        protected override void ProvideDocuments()
        {
            Console.WriteLine("Current Account: Provide Documents");
        }
    }
}
