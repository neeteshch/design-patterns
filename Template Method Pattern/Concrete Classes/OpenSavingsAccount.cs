﻿using ConsoleApp1.Template_Method_Pattern.Template_Method;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Template_Method_Pattern.Concrete_Classes
{
    class OpenSavingsAccount : CreateAccountTemplate
    {
        protected override void FillForm()
        {
            Console.WriteLine("Savings Account: Fill Form");
        }

        protected override void GetApproval()
        {
            Console.WriteLine("Savings Account: Get Approval");
        }

        protected override void OpenAccount()
        {
            Console.WriteLine("Savings Account: Open Account");
        }

        protected override void ProvideDocuments()
        {
            Console.WriteLine("Savings Account: Provide Documents");
        }
    }
}
