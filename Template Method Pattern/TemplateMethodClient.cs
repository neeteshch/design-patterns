﻿using ConsoleApp1.Template_Method_Pattern.Concrete_Classes;
using ConsoleApp1.Template_Method_Pattern.Template_Method;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Template_Method_Pattern
{
    class TemplateMethodClient
    {
        public void CreateTemplateMethodClient()
        {
            CreateAccountTemplate savingsAccountObj = new OpenSavingsAccount();
            savingsAccountObj.CreateAccount();

            CreateAccountTemplate currentAccountObj = new OpenCurrentAccount();
            currentAccountObj.CreateAccount();

            Console.Read();
        }
    }
}
