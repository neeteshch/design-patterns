﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Template_Method_Pattern.Template_Method
{
    public abstract class CreateAccountTemplate
    {
        // Template method defines the sequence for creating a bank account
        public void CreateAccount()
        {
            FillForm();
            ProvideDocuments();
            GetApproval();
            OpenAccount();
            Console.WriteLine("Account is created..");
        }
        // Methods to be implemented by subclasses
        protected abstract void FillForm();
        protected abstract void ProvideDocuments();
        protected abstract void GetApproval();
        protected abstract void OpenAccount();
    }
}
